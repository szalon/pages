function szref(myId) {
  let idSelectNo = myId.substring(7,9);

  // check if the html is ok.
  if (document.getElementsByClassName('onDisplay').length > 1) {
    alert('there can be only one szalon on display.');
  }

  let idActiveNo = document.getElementsByClassName('onDisplay')[0].id.substring(4,6);


  function nextSz(no) {
    no = ("00" + (Number(no) + 1)).slice(-2);
    return no;
  }
  function prevSz(no) {
    no = ("00" + (Number(no) - 1)).slice(-2);
    return no;
  }

  // hide the active (old)
  document.getElementById('sz22'+idActiveNo).classList.remove('onDisplay');
  document.getElementById('sz22'+idActiveNo).classList.add('hidden');

  //display the selected (new)
  document.getElementById('sz22'+idSelectNo).classList.remove('hidden');
  document.getElementById('sz22'+idSelectNo).classList.add('onDisplay');

  //handle controls szref
  // hide clicked
  document.getElementById('szref22'+idSelectNo).classList.add('hidden');


  if (Number(idSelectNo) > Number(idActiveNo)) {
    //console.log ('füre');
    if (document.getElementById('szref22'+prevSz(idSelectNo))) { // if previous exists
      if (document.getElementById('szref22'+prevSz(prevSz(idSelectNo)))) { // if pre-previous exists
        document.getElementById('szref22'+prevSz(prevSz(idSelectNo))).classList.add('hidden'); // remove pre-previous
    }
      document.getElementById('szref22'+prevSz(idSelectNo)).classList.remove('hidden'); // show previous
  }
    if (document.getElementById('szref22'+nextSz(idSelectNo))) { //if next exists
      document.getElementById('szref22'+nextSz(idSelectNo)).classList.remove('hidden'); //show next
    }

  } else {
    //console.log ('hindere');
    if (document.getElementById('szref22'+prevSz(idSelectNo))) { // if previous exists

      document.getElementById('szref22'+prevSz(idSelectNo)).classList.remove('hidden'); // show previous
  }
    if (document.getElementById('szref22'+nextSz(idSelectNo))) { //if next exists
      document.getElementById('szref22'+nextSz(idSelectNo)).classList.remove('hidden'); //show next
      if (document.getElementById('szref22'+nextSz(nextSz(idSelectNo)))) {
      document.getElementById('szref22'+nextSz(nextSz(idSelectNo))).classList.add('hidden');
      }
    }

  }



}
